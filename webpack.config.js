var path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const config = {
    entry: [ "./src/index.js"], // входная точка - исходный файл
    output: {
        path: path.resolve(__dirname, './public'),     // путь к каталогу выходных файлов - папка public
        publicPath: '/public/',
        filename: "bundle.js"       // название создаваемого файла
    },
    resolve: {
        modules: ['node_modules', 'src'], // глобальный поиск пакетов в дирректориях
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
    module: {
        rules: [   //загрузчик для jsx
            {
                test: /\.(js|jsx)$/, // определяем тип файлов
                // include: [/(app)/, /(public)/],
                exclude: /(node_modules)/,  // исключаем из обработки папку node_modules
                loader: "babel-loader",   // определяем загрузчик
                // options: {
                //     presets: [
                //         "@babel/preset-env",
                //         "@babel/preset-react",
                //         {
                //             'plugins': ['@babel/plugin-proposal-class-properties']
                //         }
                //     ]    // используемые плагины
                // }
            },
            {
                test: /\.(sass|scss|css)$/,
                use: [{
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        //      hmr: process.env.NODE_ENV === 'development',
                    },
                },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: {
                                localIdentName: '[path]_[name]_[local]',
                                // getLocalIdent: (context, localIdentName, localName) => (
                                //     getScopedName(localName, context.resourcePath)
                                // )
                            }


                        }
                    },
                    // 'postcss-loader',
                    // 'sass-loader',
                ],
            },
            // {
            //     test: /\.svg$/,
            //     loader: 'svg-inline-loader'
            // },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        // include: [Path.join(__dirname, "src/assets")],
                        options: {
                            query: {
                                name: '/public/assets/[name].[ext]'
                            },
                            outputPath: 'assets/',
                        }
                    },
                    // {
                    //     loader: 'image-webpack-loader',
                    //     options: {
                    //         query: {
                    //             mozjpeg: {
                    //                 progressive: true,
                    //             },
                    //             gifsicle: {
                    //                 interlaced: true,
                    //             },
                    //             optipng: {
                    //                 optimizationLevel: 7,
                    //             }
                    //         }
                    //     }
                    // }
                ]
            }
        ]
    },
    devtool: "source-map",
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: 'style.css',
            chunkFilename: 'style.css',
        }),
       // new BundleAnalyzerPlugin(),
       // new MinifyPlugin(), //иногда размер больше
    ],
    devServer: {
        disableHostCheck: true,
        host: '0.0.0.0',
        port: 8081,
        historyApiFallback: {
            rewrites: [
                {from: '/favicon.png', to: '/public/favicon.png'},
                {from: '/favicon.ico', to: '/public/favicon.ico'},
                {from: /^\/locales(.+)/, to: '/public/locales$1'},
                {from: /./, to: '/public/index.html'},
            ],
        },
        // proxy: {
        //     '/REST/': 'http://backend:8000',
        //     '/MEDIA/': 'http://backend:8000',
        //     '/oauth2/login/': 'http://backend:8000',
        // }
    }
}




module.exports = config