import React from 'react'
import PropTypes from 'prop-types'
import {Link} from "react-router-dom";
import style from './style.css'

const propTypes = {
    children: PropTypes.node
};

class Layout extends React.Component {

    render() {

        return (
            <div className={style.appLayout}>
                <div className={style.appMenu}>
                    <Link to={"/"} className={style.linkToUsers}>Список авторов</Link>
                    {/*<Link to={"/login/"} className={style.linkToUsers}>Login</Link>*/}
                    {/*<Link*/}
                    {/*    to={"/secret/"}*/}
                    {/*    className={style.linkToUsers}>secret</Link>*/}
                </div>
                <div className={style.content}>
                    {this.props.children}
                </div>
            </div>
        )

    }
}

if (process.env.NODE_ENV !== "production") {
    Layout.propTypes = propTypes;
}

export default Layout