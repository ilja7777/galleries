import {createSlice, createSelector, createAsyncThunk, createAction, createReducer} from '@reduxjs/toolkit'
import {startPending, pendingFailed, success} from "../../utils/redux";

const initialState = {
    status: 'idle', //loading, success, error, idle
    data: {},
    ids: []
}
//
// export const fetchUsers = createAsyncThunk('ListUsers', async (a) => {
//     try {
//         const data = await client.get('/users/');
//         await alert()
//         return data.data;
//
//     } catch (e) {
//         throw e;
//     }
// })

const slice = createSlice({
    name: 'pages.listUsers',
    initialState,
    reducers: {
        fetchStart: startPending,
        fetchSuccess: (state, {payload}) => {
            state.ids = payload
            state.status = success
        },
        fetchFailed: pendingFailed,
    },
    extraReducers:
        {
            // [fetchUsers.pending]: startPending,
            // [fetchUsers.rejected]: pendingFailed,
            // [fetchUsers.fulfilled]: (state, action) => {
            //     console.log('fulfilled:', action)
            //     const obj = {}
            //     action.payload.map((user) => obj[user.id] = user)
            //     state.status = 'success'
            //     state.data = {...state.data, ...obj}
            //
            // }
        }
})

export const {
    fetchSuccess,
    fetchFailed,
    fetchStart,
} = slice.actions

export default slice.reducer