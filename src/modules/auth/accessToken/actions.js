import {
    fetchOneStart,
    fetchOneSuccess,
    fetchOneFailed
} from './slice'

export const fetchAccessToken = () => async dispatch => {
    try {
        dispatch(fetchManyStart())
        const data = 'parol-1'
        dispatch(fetchManySuccess(data.data))
        return data.data;
    } catch (e) {
        dispatch(fetchManyFailed(e))
    }
}