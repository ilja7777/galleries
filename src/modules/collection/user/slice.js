import {createSlice} from '@reduxjs/toolkit'
import {startPending, pendingFailed, manySuccess, oneSuccess} from "../../utils/redux";



const initialState = {
    status: 'idle', //loading, success, error, idle
    data: {},
    ids: []
}

const slice = createSlice({
    name: 'collection.user',
    initialState,
    reducers: {
        fetchOneStart: startPending,
        fetchManyStart: startPending,
        fetchOneSuccess: oneSuccess,
        fetchManySuccess: manySuccess,
        fetchOneFailed: pendingFailed,
        fetchManyFailed: pendingFailed,
    },
    extraReducers:
        {}
})


export const {
    fetchManyStart,
    fetchManySuccess,
    fetchManyFailed,
    // fetchOneStart,
    // fetchOneSuccess,
    // fetchOneFailed,
} = slice.actions

export default slice.reducer
