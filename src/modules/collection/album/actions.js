import {
    // fetchOneStart,
    // fetchOneSuccess,
    // fetchOneFailed,
    fetchManyStart,
    fetchManySuccess,
    fetchManyFailed,
    addPhotosInAlbums
} from './slice'
import {fetchAlbums} from "api";


export {addPhotosInAlbums};

export const fetchListAlbums = () => async dispatch => {
    try {
        dispatch(fetchManyStart())
        const data = await fetchAlbums();
        dispatch(fetchManySuccess(data.data))
        return data.data;
    } catch (e) {
        dispatch(fetchManyFailed(e))
        throw(e.toString())
    }
}