import client from "../client/index";
import {useSelector} from "react-redux";

// export async function fetchUsers() {
//     console.log('fetchUsers1:', useSelector(selectStatusPageListUsers))
//     const data = await client.get('/users/');
//     console.log('fetchUsers2:', useSelector(selectStatusPageListUsers))
//     return data;
// }

export const fetchUsers = async () => {
    const data = await client.get('/users/');
    return data;
}

export const fetchUserById = async (id) => {
    const data = await client.get(`/users/${id}/`);
    return data.data;
}

export const fetchAlbums = async () => {
    const data = await client.get('/albums/');
    return data;
}

export const fetchAlbumById = async (id) => {
    const data = await client.get(`/albums/${id}/`);
    return data.data;
}

export const fetchPhotos = async () => {
    const data = await client.get('/photos/');
    return data;
}

export const fetchPhotoById = async (id) => {
    const data = await client.get(`/photos/${id}/`);
    return data.data;
}

