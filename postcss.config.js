// npm install postcss-loader autoprefixer css-mqpacker cssnano --save-dev
// autoprefixer - проставляет префиксы стилям
// css-mqpacker - сжимает медиа запросы
// cssnano - максимально минифицирует исходные стили
// Для работы автопрефиксера в файле package.json пропишем следующее:
//
//     "browserslist": [
//     "> 1%",
//     "last 3 version"
// ],
module.exports = {
    plugins: [
        require('autoprefixer'),
        require('css-mqpacker'),
        require('cssnano')({
            preset: [
                'default', {
                    discardComments: {
                        removeAll: true,
                    }
                }
            ]
        })
    ]
}