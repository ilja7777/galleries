// import _ from "lodash";

import {get as _get, map as _map} from "lodash";
import ListItem from "../../component/ListItem";
import React from "react";
import {selectAlbumsByIdsAlbums, selectPhotosByIdsAlbums} from "../../collection/album/selectors";
import {createSelector} from "@reduxjs/toolkit";

export const selectIdsAlbumInListAlbums = (state) => state.pages.listAlbums.ids
export const selectStatusPageListAlbums = (state) => state.pages.listAlbums.status

export const selectDataListAlbums = createSelector(
    selectAlbumsByIdsAlbums,
    selectPhotosByIdsAlbums,
    (allowAlbums,allowPhotoById,c,d) => {
        return _map(allowAlbums, (val, i) =>  {
            return {
                url: `/user/${val.userId}/album/${val.id}/`,
                title: val.title,
                imgSrc: _get(allowPhotoById, `${val.id}[0].thumbnailUrl`, '#'),
                countPhotos: _get(allowPhotoById, `${val.id}.length`, '0'),
                desc: "Фотографий: "+_get(allowPhotoById, `${val.id}.length`, '0')
            }
        })
    }
)
