//HOC
import React from "react";
import {failed, idle, pending, success} from "../utils/redux";
import Spinner from "../component/Spinner";

const withData = (View) => {
    return class extends React.Component {
        async componentDidMount() {
            //console.log('componentDidMount ItemList:')
            await this.props.getData(this.props.status)
        }
        componentDidUpdate(prevProps, prevState, snapshot) {
            //функция в которой можно проверить, какие свойства изменились

        }

        render() {
            const {status, items, title} = this.props
            const spinner = status in {pending, idle} ? <Spinner/> : null
            const errorView = status in {failed} ? <div>Error Loading</div> : null;
            if (status in {success}) {
                return <View {...this.props}/>
            }
            return <React.Fragment>
                {spinner}
                {errorView}
            </React.Fragment>

        }

    }

}
// Comp.propTypes = {
//     name: (props, propName, compName) => {
//         ...
//     }
// }
export default withData