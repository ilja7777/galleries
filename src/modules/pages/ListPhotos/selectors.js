// import _ from "lodash";

import {createSelector} from "@reduxjs/toolkit";
import {selectAlbumsByIdsAlbums, selectPhotosByIdsAlbums} from "../../collection/album/selectors";
import {get as _get, map as _map} from "lodash";
import {selectDataPhotoByIds} from "../../collection/photo/selectors";

export const selectIdsPhotosInListPhotos = (state) => state.pages.listPhotos.ids
export const selectStatusPageListPhotos = (state) => state.pages.listPhotos.status


export const selectDataListPhotos = createSelector(
    selectDataPhotoByIds,
    (items) => {
        return _map(items, (val, i) =>  {
            return {
                ...val,
                url: "#",
                imgSrc: val.thumbnailUrl,
            }
        })
    }
)