import React from 'react'
import style from './style.css'

function Spinner(props) {
    return (
        <div className={style.loadingioSpinnerDoubleRingG23zvisbvw}>
            <div className={style.ldioL52uh74fmc9}>
                <div></div>
                <div></div>
                <div>
                    <div></div>
                </div>
                <div>
                    <div></div>
                </div>
            </div>
        </div>
    )
}

export default Spinner
