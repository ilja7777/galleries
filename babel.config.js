module.exports = {
    presets: [[
        '@babel/preset-env',
        {
            loose: true,
            // with fast-async
            "exclude": ["transform-async-to-generator", "transform-regenerator"]
        }
        // {
        //     targets: {
        //         esmodules: true,
        //     },
        // },
    ],
        "@babel/preset-react",
    ],    // используемые плагины
    'plugins': [
        ['@babel/plugin-proposal-class-properties', {loose: true}],
        "@babel/plugin-syntax-dynamic-import",

        //async/await regeneragors
        // Uncaught ReferenceError: regeneratorRuntime is not defined
        ["@babel/plugin-transform-runtime",
            {
                "regenerator": true
            }
        ],
        // with fast-async
        // ["module:fast-async", { "spec": true }],
        //
        // "@babel/plugin-proposal-object-rest-spread"
    ]
}