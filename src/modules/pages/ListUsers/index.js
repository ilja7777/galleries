import React from 'react'
import {connect} from "react-redux"
import './ListUsers.module.css'
import styles from './ListUsers.module.css'
import {Link} from 'react-router-dom'
import {getListUsers} from "./actions";
import {selectAllowUsers, selectStatusPageListUsers} from "./selectors";
import _ from "lodash";
import {pending, success, idle, failed} from "../../utils/redux";
import Spinner from "../../component/Spinner";
import withData from "../../hoc-helpers/withData";

class ListUsers extends React.Component {
    renderItems(arr) {
        return _.map(arr, (val) => (
            <div
                // className={style.listUsersPageItem}
                 key={`item-user-${val.id}`}>
                <Link to={`/user/${val.id}/`}>{val.name}</Link>
            </div>)
        )
    }

    render() {
        const {status, allowUsers} = this.props
        const content = status in {success} ? this.renderItems(allowUsers) : null
        console.log('status:',status)
        return (<div className={styles.listUsersPageRoot}>
            {content}
        </div>)

    }
}


const stateToProps = (state) => {
    const status = selectStatusPageListUsers({...state})
    if (status == success) {
        return {
            status,
            allowUsers: selectAllowUsers(state)
        }
    }
    return {status}
}

const actionsToProps = (dispatch) => {
    return {
        getData: (status) => {
            if (status == idle) {
                return dispatch(getListUsers())
            }
        },
        getListUsers: () => dispatch(getListUsers())
    }
};

export default connect(stateToProps, actionsToProps)(withData(ListUsers));
