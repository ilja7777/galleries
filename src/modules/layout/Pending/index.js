import React from 'react'
import logo from './logo.svg'
import style from './style.css'

function PendingLayout(props) {
    return (
        <div className={style.pendingLayout}>
            <img
                src={logo}
                className={style.pendingLogo}
                alt="logo"/>
            <p>Loading...</p>
        </div>
    )
}

export default PendingLayout
