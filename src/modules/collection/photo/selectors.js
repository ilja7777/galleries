import {map as _map, filter as _filter} from "lodash";


export const selectDataCollectionPhoto = (state) => state.collection.photo.data
export const selectStatusCollectionPhoto = (state) => state.collection.photo.status
export const selectIdsCollectionPhoto = (state) => state.collection.photo.ids
export const selectDataCollectionPhotoByAlbumId = (state, albumId) =>
    _filter(selectDataCollectionPhoto(state), {albumId})

export const selectDataPhotoByIds = (state, ids) => _map(ids, id => state.collection.photo.data[id])

export const getIdsByData = (state, data) => _map(state, (val) => val.id)
