import {
    createSlice,
    // createSelector,
    // createAsyncThunk,
    // createAction,
    // createReducer
} from '@reduxjs/toolkit'
import {startPending, pendingFailed, oneSuccess, manySuccess} from "../../utils/redux";

const initialState = {
    status: 'idle', //loading, success, error, idle
    data: {},
    ids: []
}

const slice = createSlice({
    name: 'collection.photo',
    initialState,
    reducers: {
        fetchOneStart: startPending,
        fetchManyStart: startPending,
        fetchOneSuccess: oneSuccess,
        fetchManySuccess: manySuccess,
        fetchOneFailed: pendingFailed,
        fetchManyFailed: pendingFailed,
    },
    extraReducers:
        {}
})


export const {
    fetchManyStart,
    fetchManySuccess,
    fetchManyFailed,
    fetchOneStart,
    fetchOneSuccess,
    fetchOneFailed,
} = slice.actions





export default slice.reducer
