import React from 'react'
import logo from './404.jpg'
import style from './style.css'

class Error404Page extends React.Component {
    render() {
        return (<div
            style={{textAlign: "center", padding: "5px"}}
            className={style.notFound}>
            <div>Страница не найдена!!!</div>
            <img
                src={logo}
                alt="logo"
            />
        </div>)
    }

}

export default Error404Page