import {combineReducers} from "redux";
import {persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage';
import accessTokenReducer from "./accessToken/reducers";


const authPersistConfig = {
    key: 'auth',
    storage: storage,
    version: 0,
    whitelist: ['accessToken']
}

const authReducer = combineReducers({
    accessToken: accessTokenReducer
})

export default persistReducer(authPersistConfig, authReducer)