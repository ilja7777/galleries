import React from 'react'
import {Provider} from "react-redux";
// import {PersistGate} from "redux-persist/integration/react";
import Router from './router'
import style from './style.css'
import PropTypes from "prop-types";
import Spinner from "../modules/component/Spinner";
// import ListItem from "../modules/component/ListItem";





function App(props) {
    return (
        <Provider store={props.store}>
            <div className={style.App}>
                <Router/>
            </div>
        </Provider>
    )
}

const propTypes = {
    store: PropTypes.object.isRequired,
    persistor: PropTypes.object.isRequired,
};
if (process.env.NODE_ENV !== "production") {
    App.propTypes = propTypes;
}
export default App
