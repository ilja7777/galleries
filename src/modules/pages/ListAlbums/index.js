import React from 'react'
import style from './style.css'
import PendingLayout from "../../layout/Pending";
import ListItem from "../../component/ListItem";
// import Error404Page from "../404";
import {connect} from "react-redux";
import {selectDataListAlbums, selectIdsAlbumInListAlbums, selectStatusPageListAlbums} from "./selectors";
import {getListAlbums} from "./actions";
import _ from "lodash";
import {pending, success} from "modules/utils/redux";
import {selectAlbumsByIdsAlbums, selectPhotosByIdsAlbums} from "modules/collection/album/selectors";
import {selectUsers} from "modules/collection/user/selectors";
import ItemList from "../../component/ItemList";
import withData from "../../hoc-helpers/withData";


class ListAlbums extends React.Component {
    render() {
        const {status, data} = this.props
        const userId = parseInt(this.props.match.params.idUser);
        return (<ItemList
            getData={() => this.props.dispatch(getListAlbums({userId}))}
            items={data}
            status={status}
            title={"Список альбомов автора"}
            titleItemsNone={"У данного автора пока еще нет альбомов!"}
        />)
    }
}

const stateToProps = (state) => {
    const status = selectStatusPageListAlbums(state)
    if (status !== success)
        return {status}
    const ids = selectIdsAlbumInListAlbums(state)
    return {
        status,
        data: selectDataListAlbums(state, ids)
    };
}

const actionsToProps = (dispatch) => {
    return {
        dispatch,
    }
};

export default connect(stateToProps, actionsToProps)(ListAlbums);