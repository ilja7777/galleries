import {map as _map} from "lodash";
// export const selectUsers = state => {
//     return state.collection.user.data
// }
export const selectUsers = (state) => state.collection.user.data
// export const selectUsersByIds = createSelector(
//     selectUsers,
//     (state, ids = []) => ids,
//     (users, ids) => ids.map(id => users[id])
// )


export const selectUserById = (state, userId) => {
    return selectUsers(state)[userId]
    // state.collection.user.data
}

// export const selectUserById = createSelector(
//     selectUsers,
//     (state, id) => id,
//     (users, id) => users[id]
// )


// export const selectStatusCollectionUser = (state) => {
//     return state.collection.user.status
//     // state.collection.user.data
// }

export const selectStatusCollectionUser = (state) => state.collection.user.status
// export const selectUsersByIds = (state, ids = []) => {
//     const users = [];
//     ids.map(id => users.push(selectUserById(state, id)))
//     return users
// }
export const selectUsersByIds = (state, ids = []) => _map(ids, id => selectUserById(state, id))

