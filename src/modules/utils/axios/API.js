import axios from "axios";
import config from 'config';

export default axios.create({
    baseURL: `${config.core.APIHost}${config.core.commonPath}`,
    responseType: "json"
});