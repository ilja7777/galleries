import {combineReducers} from "redux";
import listUsersReducer from "./ListUsers/reducers";
import listAlbumsReducer from "./ListAlbums/reducers";
import listPhotosReducer from "./ListPhotos/reducers";


export default combineReducers({
    listUsers: listUsersReducer,
    listAlbums: listAlbumsReducer,
    listPhotos: listPhotosReducer
})