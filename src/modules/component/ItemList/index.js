import React from 'react'
import _ from "lodash";
import style from './style.css'
import ListItem from "../../component/ListItem";
import {failed, idle, pending, success} from "modules/utils/redux";
import Spinner from "../Spinner";
import PropTypes from "prop-types";
import App from "../../../App";
import withData from "../../hoc-helpers/withData";


class ItemList extends React.Component {
    renderItems(arr) {
        return _.map(arr, (val, i) => (
            <ListItem
                key={"list-item" + i}
                href={val.url}
                title={val.title}
                imgSrc={val.imgSrc}
                desc={val.desc}
                onClick={this.props.onItemSelected(i)}
            />
        ))
    }

    componentDidCatch(error, errorInfo) {
        console.log('error componentDidCatch:', error)
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.props.items === nextProps.items) {
            return false;
        }
        return true
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        //console.log('componentDidUpdate')
    }

    render() {
        // React.Children.map()
        // React.cloneElement()
        const {status, items, title} = this.props
        const content = status in {success} ? this.renderItems(items) : null
        return (<div className={style.listRoot}>
            <h3>{title}: </h3>
            <div className={style.listContainer}>

                {content}

                {/*{items.length && !items.length && titleItemsNone}*/}
            </div>
        </div>)
    }
}


const propTypes = {
    items: PropTypes.array,
    title: PropTypes.string.isRequired,
    titleItemsNone: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    onItemSelected: PropTypes.func,
    getData: PropTypes.func.isRequired
};
ItemList.defaultProps = {
    onItemSelected: (i) => {
        return () => null;
    }
}
if (process.env.NODE_ENV !== "production") {
    ItemList.propTypes = propTypes;
}

export default withData(ItemList)