import React, {useEffect, useState} from 'react'
import {connect, useSelector, useDispatch, shallowEqual} from "react-redux"
import {Link} from "react-router-dom";
import PendingLayout from "modules/layout/Pending";
import ListItem from "modules/component/ListItem";
import Slides from "modules/component/Slides";
import {selectDataPhotoByIds} from "modules/collection/photo/selectors";
// import Error404Page from "../404";
import {pending, success, idle} from "modules/utils/redux";
import style from './style.css'
import {selectDataListPhotos, selectIdsPhotosInListPhotos, selectStatusPageListPhotos} from "./selectors";
import {getListPhotos} from "./actions";
import Spinner from "../../component/Spinner";
import ItemList from "../../component/ItemList";


const ListAlbums = (props) => {
    const {status, items, data} = props
    const userId = parseInt(props.match.params.idUser)
    const albumId = parseInt(props.match.params.idAlbum)

    const [openedId, setOpenedId] = useState(null)
    return (<React.Fragment>
        {openedId && (<Slides
            onClose={() => setOpenedId(null)}
            onPrev={() => setOpenedId(openedId <= 1 ? items.length : openedId - 1)}
            onNext={() => setOpenedId(openedId >= items.length ? 1 : openedId + 1)}
            desc={items[openedId - 1].title}
            currentItem={`${openedId}/${items.length}`}
            url={items[openedId - 1].url}
        />)}
        <ItemList
            status={status}
            titleItemsNone={"В данном альбоме еще нет фотографий! "}
            title={"Список фотографий"}
            onItemSelected={(i) => (e) => {
                setOpenedId(i + 1);
                return e.preventDefault();
            }}
            getData={() => props.dispatch(getListPhotos({
                userId,
                albumId
            }))}
            items={data}
        />

    </React.Fragment>)
}

const stateToProps = (state) => {
    const status = selectStatusPageListPhotos(state)
    if (status !== success)
        return {status}
    const ids = selectIdsPhotosInListPhotos(state)
    return {
        status,
        data: selectDataListPhotos(state, ids),
        items: selectDataPhotoByIds(state, ids),
    };
}

const actionsToProps = (dispatch) => {
    return {
        dispatch,
    }
};

export default connect(stateToProps, actionsToProps)(ListAlbums);