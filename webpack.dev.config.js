var path = require('path');
const webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


const defaultInclude = path.resolve(__dirname, 'src')


const config = {
    entry: ["./src/index.js"], // входная точка - исходный файл
    output: {
        path: path.resolve(__dirname, './public'),     // путь к каталогу выходных файлов - папка public
        publicPath: '/public/',
        filename: "bundle.js"       // название создаваемого файла
    },
    resolve: {
        modules: ['node_modules', 'src'], // глобальный поиск пакетов в дирректориях
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
    module: {
        rules: [   //загрузчик для jsx
            {
                test: /\.(jsx?)$/, // определяем тип файлов
                // include: [/(app)/, /(public)/],
                exclude: /(node_modules)/,
                loader: "babel-loader",
                include: defaultInclude
            },
            {
                test: /\.(css)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                ],
                include: defaultInclude
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        query: {
                            name: '/public/assets/[name]__[hash:base64:5].[ext]'
                        },
                        outputPath: 'assets/',
                    }
                }],
                include: defaultInclude
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [{
                    loader: 'file-loader?name=font/[name]__[hash:base64:5].[ext]',
                    options: {
                        query: {
                            name: '/public/assets/[name]__[hash:base64:5].[ext]'
                        },
                        outputPath: 'assets/',
                    }
                }],
                include: defaultInclude
            },
            // {
            //     test: /\.svg$/,
            //     loader: 'svg-inline-loader'
            // },

        ]
    },
    devtool: "source-map",
    plugins: [
        new HtmlWebpackPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),
        new BundleAnalyzerPlugin(),
    ],
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        disableHostCheck: true,
        host: '0.0.0.0',
        port: 8081,
        historyApiFallback: {
            rewrites: [
                {from: '/favicon.png', to: '/public/favicon.png'},
                {from: '/favicon.ico', to: '/public/favicon.ico'},
                {from: /./, to: '/public/index.html'},
            ],
        },
    }
}


module.exports = config