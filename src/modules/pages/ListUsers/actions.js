import {
    fetchStart, fetchSuccess, fetchFailed
} from './slice'
import {idle} from "../../utils/redux";
import {selectStatusCollectionUser, selectUsers} from "../../collection/user/selectors";
import {fetchListUsers} from "../../collection/user/actions";
import _ from "lodash";

export const getListUsers = () => async (dispatch, getState) => {
    try {
        dispatch(fetchStart())
        let ids = []
        let users = {}
        if (selectStatusCollectionUser(getState()) == 'idle') {
            users = await dispatch(fetchListUsers());
        } else {
            users = selectUsers(getState())
            //users = _.get(getState(), 'collection.user.data', {})
        }
        ids = _.map(users, 'id')
        dispatch(fetchSuccess(ids))
        return users
    } catch (e) {
        dispatch(fetchFailed(e))
    }
}