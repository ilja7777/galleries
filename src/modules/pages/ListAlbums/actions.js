import {
    fetchStart, fetchSuccess, fetchFailed
} from './slice'
import {
    getIdsByData,
    selectDataCollectionAlbum,
    selectDataCollectionAlbumByUserId,
    selectStatusCollectionAlbum
} from "../../collection/album/selectors";
import {idle} from "../../utils/redux";
import {fetchListAlbums} from "../../collection/album/actions";
import {getListUsers} from "../ListUsers/actions";
import {selectStatusCollectionPhoto} from "../../collection/photo/selectors";
import {fetchListPhotos} from "../../collection/photo/actions";


export const getListAlbums = (filter) => async (dispatch, getState) => {
    try {
        const {userId, albumId} = filter
        dispatch(fetchStart())
        let ids = []
        let albums = {}
        let users = {}
        let photos = {}
        if (selectStatusCollectionAlbum(getState()) == idle) {
            albums = await dispatch(fetchListAlbums());
        }

        if (!!userId) {
            users = await dispatch(getListUsers());
            if (!((userId) in users)) {
                throw("user not found");
            }
            albums = selectDataCollectionAlbumByUserId(getState(), userId)
        } else {
            albums = selectDataCollectionAlbum(getState())
        }
        if (!!albumId && !((albumId) in albums)) {
            throw("album not found");
        }
        ids = getIdsByData(albums)
        if (selectStatusCollectionPhoto(getState()) == idle) {
            photos = await dispatch(fetchListPhotos());
        }

        dispatch(fetchSuccess(ids))
        return albums
    } catch (e) {
        dispatch(fetchFailed(e));
        throw e;
    }
}