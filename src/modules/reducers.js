import {combineReducers} from "redux";
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage';

import pagesReducer from "modules/pages/reducer"
import collectionReducer from "modules/collection/reducers"
import authReducer from "modules/auth/reducers"

const persistConfig = {
    key: 'root',
    storage,
    version: 0,
    whitelist: ['auth'],
    blacklist: [],
}

const rootReducer = combineReducers({
    auth: authReducer,
    pages: pagesReducer,
    collection: collectionReducer
})

// const persistedReducer = persistReducer(persistConfig, rootReducer)

export default persistReducer(persistConfig, rootReducer);