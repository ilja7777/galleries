import React from "react";
import PropTypes from 'prop-types'
import style from './style.css';
import {Link} from "react-router-dom";

const propTypes = {
    title: PropTypes.string,
    desc: PropTypes.string,
    imgSrc: PropTypes.string,
    href: PropTypes.string.isRequired
};

class ListItem extends React.Component {
    render() {
        const {title, href, imgSrc, desc, ...otherProps} = this.props;

        return (
            <Link to={href} className={style.listItemRoot} {...otherProps}>
                <div className={style.listItemLayout}>
                    {title && (<div className={style.listItemTitle}>{title}</div>)}
                    <span className={style.listItemCover}>
                        <img src={imgSrc} alt={"itemList"}/>
                    </span>
                    {desc && (<div className={style.listItemDesc}>{desc}</div>)}
                </div>
            </Link>)
    }
}

if (process.env.NODE_ENV !== "production") {
    ListItem.propTypes = propTypes;
}
export default ListItem
