import {createSlice, createSelector, createAsyncThunk, createAction, createReducer} from '@reduxjs/toolkit'
import {startPending, pendingFailed, success, idle} from "modules/utils/redux";

const initialState = {
    status: 'idle', //loading, success, error, idle
    data: {},
    ids: []
}


const slice = createSlice({
    name: 'pages.listAlbums',
    initialState,
    reducers: {
        fetchStart: startPending,
        successOne: (state, {payload}) => {
            state.status = success
            state.data = {...state.data, [payload.id]: payload}
        },
        fetchSuccess: (state, {payload}) => {
            state.ids = payload
            state.status = success
        },
        fetchFailed: pendingFailed,
    },
    extraReducers: {}
})

export const {
    fetchSuccess,
    fetchFailed,
    fetchStart
} = slice.actions





export default slice.reducer