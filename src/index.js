import React from 'react'
import {render} from 'react-dom'
import './index.css'
import App from "./App";
import reportWebVitals from './reportWebVitals'

import configureAppStore from "./store";

const {store, persistor} = configureAppStore()
const renderApp = () =>
    render(
        <React.StrictMode>
            <App store={store} persistor={persistor}/>
        </React.StrictMode>,
        document.getElementById('root')
    )

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()

// if (process.env.NODE_ENV !== 'production' && module.hot) {
//     module.hot.accept('./', renderApp)
// }

renderApp()