import {
    createSlice,
    // createSelector,
    // createAsyncThunk,
    // createAction,
    // createReducer
} from '@reduxjs/toolkit'
import {fetchPhotos} from "api";
import {startPending, pendingFailed, oneSuccess, manySuccess} from "../../utils/redux";
import _ from "lodash";
import {addPhotosInAlbums} from "modules/collection/album/actions";


const initialState = {
    status: 'idle', //loading, success, error, idle
    data: {},
    ids: []
}

const photoSlice = createSlice({
    name: 'collection.photo',
    initialState,
    reducers: {
        fetchOneStart: startPending,
        fetchManyStart: startPending,
        fetchOneSuccess: oneSuccess,
        fetchManySuccess: manySuccess,
        fetchOneFailed: pendingFailed,
        fetchManyFailed: pendingFailed,
    },
    extraReducers:
        {}
})


export const {
    fetchManyStart,
    fetchManySuccess,
    fetchManyFailed,
    fetchOneStart,
    fetchOneSuccess,
    fetchOneFailed,
} = photoSlice.actions


export const selectDataCollectionPhoto = (state) => state.collection.photo.data
export const selectStatusCollectionPhoto = (state) => state.collection.photo.status
export const selectIdsCollectionPhoto = (state) => state.collection.photo.ids
export const selectDataCollectionPhotoByAlbumId = (state, albumId) =>
    _.filter(selectDataCollectionPhoto(state), {albumId})

export const selectDataPhotoByIds = (state, ids) => _.map(ids, id => state.collection.photo.data[id])

export const getIdsByData = (state, data) => _.map(state, (val) => val.id)


export const fetchListPhotos = () => async dispatch => {
    try {
        await dispatch(fetchManyStart())
        const data = await fetchPhotos();
        await dispatch(fetchManySuccess(data.data))
        await dispatch(addPhotosInAlbums(data.data))

        return data.data;
    } catch (e) {
        await dispatch(fetchManyFailed(e))
    }
}

const photoReducer = photoSlice.reducer
export default photoReducer
