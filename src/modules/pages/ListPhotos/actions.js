import {
    fetchStart, fetchSuccess, fetchFailed
} from './slice'
import {
    getIdsByData,
    selectDataCollectionAlbum,
    selectDataCollectionAlbumByUserId,
} from "../../collection/album/selectors";
import {idle} from "../../utils/redux";
import {getListUsers} from "../ListUsers/actions";
import {
    selectDataCollectionPhoto,
    selectDataCollectionPhotoByAlbumId,
    selectStatusCollectionPhoto
} from "../../collection/photo/selectors";
import {fetchListPhotos} from "../../collection/photo/actions";
import {getListAlbums} from "../ListAlbums/actions";



export const getListPhotos = (filter={}) => async (dispatch, getState) => {
    try {
        const {albumId, userId} = filter
        dispatch(fetchStart())
        let ids = []
        let albums = {}
        let users = {}
        let photos = {}

        if (selectStatusCollectionPhoto(getState()) == 'idle') {
            photos = await dispatch(fetchListPhotos());
        }
        if (albumId) {
            albums = await dispatch(getListAlbums({albumId}));
            photos = selectDataCollectionPhotoByAlbumId(getState(), filter.albumId)
        } else {
            photos = selectDataCollectionPhoto(getState())
        }
        if (!!userId) {
            users = await dispatch(getListUsers());
            if (!((userId) in users)) {
                throw("user not found");
            }
            albums = selectDataCollectionAlbumByUserId(getState(), userId)
        } else {
            albums = selectDataCollectionAlbum(getState())
        }
        if (selectStatusCollectionPhoto(getState()) == idle) {
            photos = await dispatch(fetchListPhotos());
        }
        ids = getIdsByData(photos)
        dispatch(fetchSuccess(ids))
        return albums
    } catch (e) {
        dispatch(fetchFailed(e))
        throw e;
    }
}