import {createSlice} from '@reduxjs/toolkit'
import {startPending, pendingFailed, success, idle} from "modules/utils/redux";

const initialState = {
    status: 'idle', //loading, success, error, idle
    data: {},
    ids: []
}


const slice = createSlice({
    name: 'pages.listPhotos',
    initialState,
    reducers: {
        fetchStart: startPending,
        fetchSuccess: (state, {payload}) => {
            state.ids = payload
            state.status = success
        },
        fetchFailed: pendingFailed,
    },
    extraReducers: {}
})

export const {
    fetchSuccess,
    fetchFailed,
    fetchStart
} = slice.actions


export default slice.reducer