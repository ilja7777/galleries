import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route, Link,
    //  Link
} from 'react-router-dom'
import Layout from 'modules/layout'
import ListUsers from 'modules/pages/ListUsers'
import ListAlbums from 'modules/pages/ListAlbums'
import ListPhotos from "modules/pages/ListPhotos";
import Error404Page from "modules/pages/404";
import LoginPage from "../modules/pages/LoginPage";
import SecretPage from "../modules/pagesPrivate/secretPage";


class AppRouter extends React.PureComponent {
    state = {
        isLoggedIn: false
    };

    render() {
        return (
            <Router>
                <Layout>
                    <Switch>
                        <Route path={'/'} exact component={ListUsers}/>
                        {/*<Route*/}
                        {/*    path={'/login/'} exact*/}
                        {/*    render={*/}
                        {/*        () => (<LoginPage isLoggedIn={this.state.isLoggedIn} onLogin={() => {*/}
                        {/*        this.setState({isLoggedIn: true})}}/>)*/}
                        {/*    }*/}
                        {/*/>*/}
                        {/*<Route*/}
                        {/*    path={'/secret/'}*/}
                        {/*    exact*/}
                        {/*    render={() => (<SecretPage isLoggedIn={this.state.isLoggedIn}/>)}*/}
                        {/*/>*/}
                        <Route path={"/user/:idUser(\\d+)/"} exact component={ListAlbums}/>
                        <Route path={'/user/:idUser(\\d+)/album/:idAlbum(\\d+)/'} exact component={ListPhotos}/>
                        <Route path={"*"} exact component={Error404Page}/>
                    </Switch>
                </Layout>
            </Router>
        )
    }
}

export default AppRouter