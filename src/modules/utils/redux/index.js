export const idle = 'idle'
export const success = 'success'
export const failed = 'failed'
export const pending = 'pending'

export const startPending = (state) => {
    state.status = pending
}
export const pendingFailed = (state, {payload, error}) => {
    state.status = failed
    if (payload)
        state.error = payload.toString()
    if (error)
        state.error = error.toString()
}

export const manySuccess = (state, action) => {
    let data = {}
    const ids = action.payload.map((val) => {
        data[val.id] = val
        return val.id
    })
    state.status = success
    state.ids = ids
    state.data = data
}
export const oneSuccess = (state, {payload}) => {
    state.status = success
    state.data = {...state.data, [payload.id]: payload}
    state.ids.push(payload.id)
}


