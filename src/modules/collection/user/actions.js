import {
    // fetchOneStart,
    // fetchOneSuccess,
    // fetchOneFailed,
    fetchManyStart,
    fetchManySuccess,
    fetchManyFailed,
} from './slice'
import {fetchUsers} from "api";


export const fetchListUsers = () => async (dispatch, getState) => {
    try {
        dispatch(fetchManyStart())
        const data = await fetchUsers()
        dispatch(fetchManySuccess(data.data))
        return data.data;
    } catch (e) {
        dispatch(fetchManyFailed(e))
        throw(e.toString())
    }
}