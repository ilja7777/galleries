import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit'
import {persistStore, persistReducer} from 'redux-persist'
// import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
// import storageSession from 'redux-persist/lib/storage/session'



import rootReducer from 'modules/reducers'

const preloadedState = {};

export default function configureAppStore(preloadedState) {
    const middleware = getDefaultMiddleware({
        immutableCheck: false,
        //не отображать время получения элементов в консоле
        serializableCheck: false,
        //thunk: true,
    });

    const store = configureStore({
        reducer: rootReducer,
        middleware: [...middleware],
        preloadedState,
        enhancers: [],
        devTools: process.env.NODE_ENV !== 'production',
    })

// if (process.env.NODE_ENV !== 'production' && module.hot) {
//     module.hot.accept('./reducers', () => store.replaceReducer(rootReducer))
// }
    let persistor = persistStore(store)
    return {store, persistor}
}

