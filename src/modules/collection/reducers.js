import {combineReducers} from "redux";
import userReducer from "./user/reducers";
import albumReducer from "./album/reducers";
import photoReducer from "./photo/reducers";


export default combineReducers({
    user: userReducer,
    album: albumReducer,
    photo: photoReducer
})