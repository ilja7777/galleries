import {createSlice} from '@reduxjs/toolkit'
import {startPending, pendingFailed, oneSuccess, manySuccess} from "modules/utils/redux";


const initialState = {
    status: 'idle', //loading, success, error, idle
    data: {},
}

const slice = createSlice({
    name: 'auth.accessToken',
    initialState,
    reducers: {
        fetchOneStart: startPending,
        fetchOneSuccess: oneSuccess,
        fetchOneFailed: pendingFailed,
    },
    extraReducers:
        {}
})


export const {
    fetchOneStart,
    fetchOneSuccess,
    fetchOneFailed,
} = slice.actions
export default slice.reducer
