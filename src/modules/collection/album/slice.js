import {createSlice} from '@reduxjs/toolkit'
import {startPending, pendingFailed, oneSuccess, manySuccess} from "modules/utils/redux";

const initialState = {
    status: 'idle', //loading, success, error, idle
    data: {},
    ids: [],
    relationIdsPhotoById: {}
}

const slice = createSlice({
    name: 'collection.album',
    initialState,
    reducers: {
        fetchOneStart: startPending,
        fetchManyStart: startPending,
        fetchOneSuccess: oneSuccess,
        fetchManySuccess: manySuccess,
        fetchOneFailed: pendingFailed,
        fetchManyFailed: pendingFailed,
        addPhotosInAlbum: (state, {payload}) => {
            state.relationIdsPhotoById[payload.albumId] = payload.id
        },
        addPhotosInAlbums: (state, {payload}) => {
            payload.map((val) => {
                state.relationIdsPhotoById[val.albumId] = [...(state.relationIdsPhotoById[val.albumId] || []), val.id]
            })
        }
    },
    extraReducers:
        {}
})

export const {
    fetchManyStart,
    fetchManySuccess,
    fetchManyFailed,
    fetchOneStart,
    fetchOneSuccess,
    fetchOneFailed,
    addPhotosInAlbums
} = slice.actions

export default slice.reducer




