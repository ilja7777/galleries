import React from 'react'
import style from './style.css'

function Slides(props) {
    return (
        <div className={style.slidesLayout}>
            <span className={style.slidesClose} onClick={props.onClose}>×</span>
            <div className={style.slidesContent}>
                <div className={style.slideCurrentIndex}>{props.currentItem}</div>
                <div className={style.slidesBody}>
                    <span className={style.slidePrev} onClick={props.onPrev}>❮</span>
                    <div className={style.slideImg} style={{backgroundImage: 'url("' + props.url + '")'}}></div>
                    <span className={style.slideNext} onClick={props.onNext}>❯</span>
                </div>
                <div className={style.slideDesc}>{props.desc}</div>
            </div>
        </div>
    )
}

export default Slides
