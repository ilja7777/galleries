import withData from "../../hoc-helpers/withData";

const compose = (...funcs) => (comp) => {
    return funcs.reduceRight((wrapped, f) => f(wrapped), comp);
}

// const MyComp = compose(widthSevice,withData,withChild(renderName))(SimpleComponent)