export default {
    linkToUsers: {
        backgroundColor: "#4CAF50",
        border: 'none',
        color: 'white',
        padding: '15px 32px',
        textAlign: 'center',
        textDecoration: 'none',
        display: 'inline-block',
        fontSize: '16px',
        margin: '2px',
    }
}
