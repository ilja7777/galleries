import React from 'react';
// import {Redire} from 'react-router-dom'
import {Redirect} from 'react-router-dom'

const LoginPage = ({isLoggedIn, onLogin,...props}) => {
    if (isLoggedIn) {
        return (<Redirect to={"/secret/"} />)
    }
    return(
        <div>
            <p>Login to see sseecret page</p>
            <button onClick={onLogin}>Login
            </button>
        </div>
    )
}

export default LoginPage