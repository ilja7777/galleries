import {
    // fetchOneStart,
    // fetchOneSuccess,
    // fetchOneFailed,
    fetchManyStart,
    fetchManySuccess,
    fetchManyFailed,
} from './slice'
import {fetchPhotos} from "api";
import {addPhotosInAlbums} from "modules/collection/album/actions";


export const fetchListPhotos = () => async dispatch => {
    try {
        await dispatch(fetchManyStart())
        const data = await fetchPhotos();
        await dispatch(fetchManySuccess(data.data))
        await dispatch(addPhotosInAlbums(data.data))

        return data.data;
    } catch (e) {
        await dispatch(fetchManyFailed(e))
        throw(e.toString())
    }
}