// import _ from "lodash";
import {selectUsersByIds} from "../../collection/user/selectors";

export const selectStatusPageListUsers = (state) => state.pages.listUsers.status
export const selectIds = (state) => state.pages.listUsers.ids
// export const selectAllowUsers = (state) => selectUsersByIds(state, selectIds(state))
export const selectAllowUsers = (state) => selectUsersByIds(state, selectIds(state))