const webpack = require('webpack')
var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MinifyPlugin = require('babel-minify-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const getScopedName = require(path.resolve(__dirname, 'getScopedName.js'));

const defaultInclude = path.resolve(__dirname, 'src')

const config = {
    entry: ["./src/index.js"], // входная точка - исходный файл
    output: {
        path: path.resolve(__dirname, './public'),     // путь к каталогу выходных файлов - папка public
        publicPath: '/public/',
        filename: "bundle.js"       // название создаваемого файла
    },
    resolve: {
        modules: ['node_modules', 'src'], // глобальный поиск пакетов в дирректориях
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: [
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true,
                        modules: {
                            // localIdentName: '[path]_[name]_[local]',
                            getLocalIdent: (context, localIdentName, localName) => (
                                getScopedName(localName, context.resourcePath)
                            )
                        }


                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true,
                        postcssOptions: {
                            config: path.resolve(__dirname, "postcss.config.js"),
                        }
                    }
                }
            ],
            include: defaultInclude
        },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {sourceMap: true}
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                            postcssOptions: {
                                config: path.resolve(__dirname, "postcss.config.js"),
                            }
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {sourceMap: true}
                    }
                ],
                include: defaultInclude
            },
            {
                test: /\.(jsx?)$/,
                exclude: /(node_modules)/,
                loader: "babel-loader",
                // include: defaultInclude
            },

            {
                test: /\.(jpe?g|png|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        query: {
                            name: '/public/assets/[name]__[hash:base64:5].[ext]'
                        },
                        outputPath: 'assets/',
                    }
                }],
                include: defaultInclude
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [{
                    loader: 'file-loader?name=font/[name]__[hash:base64:5].[ext]',
                    options: {
                        query: {
                            name: '/public/assets/[name]__[hash:base64:5].[ext]'
                        },
                        outputPath: 'assets/',
                    }
                }],
                include: defaultInclude
            },
        ]
    },
    devtool: "source-map",
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: 'style.css',
            chunkFilename: 'style.css',
        }),
        new HtmlWebpackPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        // new MinifyPlugin(), //иногда размер больше
        // new BundleAnalyzerPlugin(),
    ],
    devServer: {
        disableHostCheck: true,
        host: '0.0.0.0',
        port: 8081,
        historyApiFallback: {
            rewrites: [
                {from: '/favicon.png', to: '/public/favicon.png'},
                {from: '/favicon.ico', to: '/public/favicon.ico'},
                {from: /^\/locales(.+)/, to: '/public/locales$1'},
                // {from: './index.html', to: '/public/index.html'},
            ],
        },
    }
}


module.exports = config