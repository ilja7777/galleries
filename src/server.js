import path from 'path'
import qs from 'qs' // Add this at the top of the file
import Express from 'express'
import {renderToString} from 'react-dom/server'
import React from 'react'
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import counterApp from './reducers'
import App from "./App";
import configureAppStore from "./store";

const app = Express()
const port = 3000

//Serve static files
app.use('/static', Express.static('static'))

// This is fired every time the server side receives a request
app.use(handleRender)

// We are going to fill these out in the sections to follow
function handleRender(req, res) {
    const params = qs.parse(req.query)
    console.log(req)
    // Create a new Redux store instance
    // const store = createStore(counterApp)

    const store = configureAppStore()
    // Render the component to a string
    const html = renderToString(<App store={store}/>)
    const preloadedState = store.getState()
    res.send(renderFullPage(html, preloadedState))
}

function renderFullPage(html, preloadedState) {
    return `
    <!doctype html>
    <html>
      <head>
        <title>Redux Universal Example</title>
      </head>
      <body>
        <div id="root">${html}</div>
        <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // https://redux.js.org/recipes/server-rendering/#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
        /</g,
        '\\u003c'
    )}
        </script>
        <script src="/static/bundle.js"></script>
      </body>
    </html>
    `
}
