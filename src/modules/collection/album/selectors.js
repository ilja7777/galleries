import _ from "lodash";

export const selectDataCollectionAlbum = (state) => state.collection.album.data
export const selectDataCollectionAlbumById = (state, id) => state.collection.album.data[id]
export const selectStatusCollectionAlbum = (state) => state.collection.album.status
export const selectIdsCollectionAlbum = (state) => state.collection.album.ids
export const selectDataCollectionAlbumByUserId = (state, userId) =>
    _.filter(selectDataCollectionAlbum(state), {userId})

export const getIdsByData = (state, data) => _.map(state, (val) => val.id)

export const selectIdsPhotoByIdAlbum = (state, albumId) =>
    state.collection.album.relationIdsPhotoById[albumId]

export const selectPhotosByIdAlbum = (state, albumId) => {
    return _.map(selectIdsPhotoByIdAlbum(state, albumId), (id) => state.collection.photo.data[id])
}


export const selectPhotosByIdsAlbums = (state, ids) => {
    let data = {}
    ids.map((id) => data[id] = selectPhotosByIdAlbum(state, id))
    return data;
}

export const selectAlbumsByIdsAlbums = (state, ids) => {
    let data = {}
    ids.map((id) => data[id] = selectDataCollectionAlbumById(state, id))
    return data;
}